package com.task.cyclepricingsolution.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Mahendra Prajapati
 * @since 17-08-2020
 */

@NoArgsConstructor
@Data
public class Wheel {
    private String spokesType;
    private Integer numberOfSpokes;
    private Double rimThickness;
    private String rimType;
    private String tyreType;
    private int monthValue;

    private Map<String, Double> spokePriceMap;
    private Map<String, Double> rimPriceMap;
    private Map<String, Double> tyrePriceMap;

    {
        spokePriceMap = new HashMap<>();
        spokePriceMap.put("steel", 10.0);
        spokePriceMap.put("iron", 6.0);

        rimPriceMap = new HashMap<>();
        rimPriceMap.put("steel", 10.0);
        rimPriceMap.put("iron", 6.0);

        tyrePriceMap = new HashMap<>();
        tyrePriceMap.put("tubeless", 230.0);
        tyrePriceMap.put("tube", 200.0);

        monthValue = LocalDate.now().getMonthValue();

    }

    public Double getWheelPrice() {
        Double finalWheelPrice;
        finalWheelPrice = numberOfSpokes * spokePriceMap.getOrDefault(spokesType, 0.0)
                + rimThickness * rimPriceMap.getOrDefault(rimType, 0.0)
                + tyrePriceMap.getOrDefault(tyreType, 0.0);
        if (getMonthValue() <= 11 && getMonthValue() >= 1)
            finalWheelPrice += 20;
        else
            finalWheelPrice += 30;

        return finalWheelPrice;
    }
}
