package com.task.cyclepricingsolution.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Mahendra Prajapati
 * @since 18-08-2020
 */

@NoArgsConstructor
@Data
public class Cycle {
    // High level Designs
    @JsonProperty("frame")
    public Frame frame;

    @JsonProperty("handleBar")
    public HandleBar handleBar;

    @JsonProperty("seat")
    public Seat seat;

    @JsonProperty("wheel")
    public Wheel wheel;

    @JsonProperty("chain")
    public Chain chain;

    // Final Price of the Cycle
    public double getCyclePrice() {
        double result;
        if (frame == null || handleBar == null || seat == null || wheel == null || chain == null)
            return 0.0;
        else
            result = frame.getFramePrice() + handleBar.getHandleBarPrice() + seat.getSeatPrice() + wheel.getWheelPrice() + chain.getChainPrice();
        return result;
    }

}
