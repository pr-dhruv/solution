package com.task.cyclepricingsolution.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Mahendra Prajapati
 * @since 17-08-2020
 */
@NoArgsConstructor
@Data
public class Chain {
    private boolean containsGears;
    private Integer numberOfGears;

    public Double getChainPrice() {
        Double result;
        if (!containsGears)
            result = 20.0;
        else
            result = numberOfGears * 25.0;
        return result;
    }

}
