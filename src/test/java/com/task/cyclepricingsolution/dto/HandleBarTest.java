package com.task.cyclepricingsolution.dto;

import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.DisplayName;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.*;

/**
 * @author Mahendra Prajapati
 * @since 19-08-2020
 */
public class HandleBarTest {

    @InjectMocks
    private HandleBar handleBar;

    @Mock
    private Map<String, Double> priceMap;

    {
        MockitoAnnotations.initMocks(this);
        priceMap = new HashMap<>();
        priceMap.put("steel", 15.0);
        priceMap.put("iron", 10.0);

    }

    @Before
    public void setup() {
        handleBar.setMaterial(null);
        handleBar.setPriceMap(priceMap);
    }

    @Test
    @DisplayName("Steel Handle Bar")
    public void getPriceForRubberSeat() {
        handleBar.setMaterial("steel");
        Double expectedPrice = handleBar.getHandleBarPrice();
        Double actualPrice = 15.0;
        assertTrue(expectedPrice.equals(actualPrice));

    }
    @Test
    @DisplayName("Iron Handle Bar")
    public void getPriceForLeather() {
        handleBar.setMaterial("iron");
        Double expectedPrice = handleBar.getHandleBarPrice();
        Double actualPrice = 10.0;
        assertTrue(expectedPrice.equals(actualPrice));
    }

    @Test
    @DisplayName("No HandleBar material Found, value = null")
    public void noSeatFoundNullValue() {
        Double expectedPrice = handleBar.getHandleBarPrice();
        Double actualPrice = 0.0;
        assertTrue(expectedPrice.equals(actualPrice));
    }

    @Test
    @DisplayName("No HandleBar material Found, value = \"\"")
    public void noSeatFoundEmptyString() {
        handleBar.setMaterial("");
        Double expectedPrice = handleBar.getHandleBarPrice();
        Double actualPrice = 0.0;
        assertTrue(expectedPrice.equals(actualPrice));
    }

}