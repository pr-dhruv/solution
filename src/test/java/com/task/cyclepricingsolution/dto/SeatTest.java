package com.task.cyclepricingsolution.dto;

import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.DisplayName;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.*;

/**
 * @author Mahendra Prajapati
 * @since 19-08-2020
 */
public class SeatTest {
    @InjectMocks
    private Seat seat;

    @Mock
    private Map<String, Double> priceMap;

    {
        MockitoAnnotations.initMocks(this);
        priceMap = new HashMap<>();
        priceMap.put("rubber", 20.0);
        priceMap.put("leather", 30.0);

    }

    @Before
    public void setup() {
        seat.setMaterial(null);
        seat.setPriceMap(priceMap);
    }

    @Test
    @DisplayName("Rubber Seat")
    public void getPriceForRubberSeat() {
        seat.setMaterial("rubber");
        Double expectedPrice = seat.getSeatPrice();
        Double actualPrice = 20.0;
        assertTrue(expectedPrice.equals(actualPrice));

    }
    @Test
    @DisplayName("Leather Seat")
    public void getPriceForLeather() {
        seat.setMaterial("leather");
        Double expectedPrice = seat.getSeatPrice();
        Double actualPrice = 30.0;
        assertTrue(expectedPrice.equals(actualPrice));
    }

    @Test
    @DisplayName("No Seat Found, value = null")
    public void noSeatFoundNullValue() {
        Double expectedPrice = seat.getSeatPrice();
        Double actualPrice = 0.0;
        assertTrue(expectedPrice.equals(actualPrice));
    }

    @Test
    @DisplayName("No Seat Found, value = \"\"")
    public void noSeatFoundEmptyString() {
        seat.setMaterial("");
        Double expectedPrice = seat.getSeatPrice();
        Double actualPrice = 0.0;
        assertTrue(expectedPrice.equals(actualPrice));
    }

}