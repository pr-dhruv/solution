package com.task.cyclepricingsolution.dto;

import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.DisplayName;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertTrue;

/**
 * @author Mahendra Prajapati
 * @since 19-08-2020
 */
public class FrameTest {

    @InjectMocks
    private Frame frame;

    @Mock
    private Map<String, Double> framePriceList;

    {
        MockitoAnnotations.initMocks(this);
        framePriceList = new HashMap<>();
        framePriceList.put("Steel", 12.0);
        framePriceList.put("iron", 10.0);

    }

    @Before
    public void setup() {
        frame.setMaterial(null);
        frame.setFramePriceList(framePriceList);
    }

    @Test
    @DisplayName("Steel Frame")
    public void getPriceForSteelFrame() {
        frame.setMaterial("Steel");
        Double expectedPrice = frame.getFramePrice();
        Double actualPrice = 12.0;
        assertTrue(expectedPrice.equals(actualPrice));

    }
    @Test
    @DisplayName("Iron Frame")
    public void getPriceForIronFrame() {
        frame.setMaterial("iron");
        Double expectedPrice = frame.getFramePrice();
        Double actualPrice = 10.0;
        assertTrue(expectedPrice.equals(actualPrice));
    }

    @Test
    @DisplayName("No Frame Found, value = null")
    public void noFramesFoundNullValue() {
        Double expectedPrice = frame.getFramePrice();
        Double actualPrice = 0.0;
        assertTrue(expectedPrice.equals(actualPrice));
    }

    @Test
    @DisplayName("No Frame Found, value = \"\"")
    public void noFramesFoundEmptyString() {
        frame.setMaterial("");
        Double expectedPrice = frame.getFramePrice();
        Double actualPrice = 0.0;
        assertTrue(expectedPrice.equals(actualPrice));
    }

}