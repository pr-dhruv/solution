package com.task.cyclepricingsolution.dto;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

/**
 * @author Mahendra Prajapati
 * @since 19-08-2020
 */
public class WheelTest {

    @InjectMocks
    private Wheel wheel;

    @Mock
    private Map<String, Double> spokePriceMap;

    @Mock
    private Map<String, Double> rimPriceMap;

    @Mock
    private Map<String, Double> tirePriceMap;

    private static Integer numberOfSpokes;

    private static Double rimThickness;

    private int month;

    {
        MockitoAnnotations.initMocks(this);
        spokePriceMap = new HashMap<>();
        spokePriceMap.put("steel", 10.0);
        spokePriceMap.put("iron", 6.0);

        rimPriceMap = new HashMap<>();
        rimPriceMap.put("Steel", 10.0);
        rimPriceMap.put("iron", 6.0);

        tirePriceMap = new HashMap<>();
        tirePriceMap.put("tubeless", 230.0);
        tirePriceMap.put("tube", 200.0);

        numberOfSpokes = 5;
        rimThickness = 17.0;

        month = LocalDate.now().getMonthValue();

    }

    @Before
    public void setup() {
        wheel.setSpokesType(null);
        wheel.setNumberOfSpokes(0);
        wheel.setRimThickness(0.0);
        wheel.setSpokePriceMap(spokePriceMap);
        wheel.setRimPriceMap(rimPriceMap);
        wheel.setTyrePriceMap(tirePriceMap);
    }

    /**
     * Test cases when Month is between 1 to 11
     */
    @Test
    public void getWheel_WithSteelSpoke_withSteelRim_withTubelessTire() {
        when(LocalDate.now().getMonthValue()).thenReturn(5);
        wheel.setSpokesType("steel");
        wheel.setNumberOfSpokes(numberOfSpokes);
        wheel.setRimThickness(rimThickness);
        wheel.setRimType("steel");
        wheel.setTyreType("tubeless");
        Double actualPrice = wheel.getWheelPrice();
        Double expectedPrice = 300.0;
        assertTrue(expectedPrice.equals(actualPrice));
    }

    @Test
    public void getWheel_withSteelSpoke_withSteelRim_withTubeTire() {
        when(LocalDate.now().getMonthValue()).thenReturn(5);
        Wheel wheel = new Wheel();
        wheel.setSpokesType("steel");
        wheel.setNumberOfSpokes(numberOfSpokes);
        wheel.setRimThickness(rimThickness);
        wheel.setRimType("steel");
        wheel.setTyreType("tube");
        Double actualPrice = wheel.getWheelPrice();
        Double expectedPrice = 270.0;
        assertTrue(expectedPrice.equals(actualPrice));
    }

    @Test
    public void getWheel_withSteelSpoke_withIronRim_withTubelessTire() {
        when(LocalDate.now().getMonthValue()).thenReturn(5);
        Wheel wheel = new Wheel();
        wheel.setSpokesType("steel");
        wheel.setNumberOfSpokes(numberOfSpokes);
        wheel.setRimThickness(rimThickness);
        wheel.setRimType("iron");
        wheel.setTyreType("tubeless");
        Double actualPrice = wheel.getWheelPrice();
        Double expectedPrice = 402.0;
        assertTrue(expectedPrice.equals(actualPrice));
    }

    @Test
    public void getWheel_withSteelSpoke_withIronRim_withTubeTire() {
        when(LocalDate.now().getMonthValue()).thenReturn(5);
        Wheel wheel = new Wheel();
        wheel.setSpokesType("steel");
        wheel.setNumberOfSpokes(numberOfSpokes);
        wheel.setRimThickness(rimThickness);
        wheel.setRimType("iron");
        wheel.setTyreType("tube");
        Double actualPrice = wheel.getWheelPrice();
        Double expectedPrice = 372.0;
        assertTrue(expectedPrice.equals(actualPrice));
    }

    @Test
    public void getWheel_withIronSpoke_withSteelRim_withTubelessTire() {
        when(LocalDate.now().getMonthValue()).thenReturn(5);
        Wheel wheel = new Wheel();
        wheel.setSpokesType("iron");
        wheel.setNumberOfSpokes(numberOfSpokes);
        wheel.setRimThickness(rimThickness);
        wheel.setRimType("steel");
        wheel.setTyreType("tubeless");
        Double actualPrice = wheel.getWheelPrice();
        Double expectedPrice = 280.0;
        assertTrue(expectedPrice.equals(actualPrice));
    }

    @Test
    public void getWheel_withIronSpoke_withSteelRim_withTubeTire() {
        when(LocalDate.now().getMonthValue()).thenReturn(5);
        Wheel wheel = new Wheel();
        wheel.setSpokesType("iron");
        wheel.setNumberOfSpokes(numberOfSpokes);
        wheel.setRimThickness(rimThickness);
        wheel.setRimType("steel");
        wheel.setTyreType("tube");
        Double actualPrice = wheel.getWheelPrice();
        Double expectedPrice = 250.0;
        assertTrue(expectedPrice.equals(actualPrice));
    }

    @Test
    public void getWheel_withIronSpoke_withIronRim_withTubelessTire() {
        when(LocalDate.now().getMonthValue()).thenReturn(5);
        Wheel wheel = new Wheel();
        wheel.setSpokesType("iron");
        wheel.setNumberOfSpokes(numberOfSpokes);
        wheel.setRimThickness(rimThickness);
        wheel.setRimType("iron");
        wheel.setTyreType("tubeless");
        Double actualPrice = wheel.getWheelPrice();
        Double expectedPrice = 382.0;
        assertTrue(expectedPrice.equals(actualPrice));
    }

    @Test
    public void getWheel_withIronSpoke_withIronRim_withTubeTire() {
        Wheel wheel = new Wheel();
        wheel.setSpokesType("iron");
        wheel.setNumberOfSpokes(numberOfSpokes);
        wheel.setRimThickness(rimThickness);
        wheel.setRimType("iron");
        wheel.setTyreType("tube");
        Double actualPrice = wheel.getWheelPrice();
        Double expectedPrice = 352.0;
        assertTrue(expectedPrice.equals(actualPrice));
    }
    /**
     * When month is 12
     */
    @Test
    public void getWheel_WithSteelSpoke_withSteelRim_withTubelessTire_Dec_Month() {
        when(LocalDate.now().getMonthValue()).thenReturn(12);
        wheel.setSpokesType("steel");
        wheel.setNumberOfSpokes(numberOfSpokes);
        wheel.setRimThickness(rimThickness);
        wheel.setRimType("steel");
        wheel.setTyreType("tubeless");
        Double actualPrice = wheel.getWheelPrice();
        Double expectedPrice = 300.0;
        assertTrue(expectedPrice.equals(actualPrice));
    }

    @Test
    public void getWheel_withSteelSpoke_withSteelRim_withTubeTire_Dec_Month() {
        when(LocalDate.now().getMonthValue()).thenReturn(12);
        Wheel wheel = new Wheel();
        wheel.setSpokesType("steel");
        wheel.setNumberOfSpokes(numberOfSpokes);
        wheel.setRimThickness(rimThickness);
        wheel.setRimType("steel");
        wheel.setTyreType("tube");
        Double actualPrice = wheel.getWheelPrice();
        Double expectedPrice = 270.0;
        assertTrue(expectedPrice.equals(actualPrice));
    }

    @Test
    public void getWheel_withSteelSpoke_withIronRim_withTubelessTire_Dec_Month() {
        when(LocalDate.now().getMonthValue()).thenReturn(12);
        Wheel wheel = new Wheel();
        wheel.setSpokesType("steel");
        wheel.setNumberOfSpokes(numberOfSpokes);
        wheel.setRimThickness(rimThickness);
        wheel.setRimType("iron");
        wheel.setTyreType("tubeless");
        Double actualPrice = wheel.getWheelPrice();
        Double expectedPrice = 402.0;
        assertTrue(expectedPrice.equals(actualPrice));
    }

    @Test
    public void getWheel_withSteelSpoke_withIronRim_withTubeTire_Dec_Month() {
        when(LocalDate.now().getMonthValue()).thenReturn(12);
        Wheel wheel = new Wheel();
        wheel.setSpokesType("steel");
        wheel.setNumberOfSpokes(numberOfSpokes);
        wheel.setRimThickness(rimThickness);
        wheel.setRimType("iron");
        wheel.setTyreType("tube");
        Double actualPrice = wheel.getWheelPrice();
        Double expectedPrice = 372.0;
        assertTrue(expectedPrice.equals(actualPrice));
    }

    @Test
    public void getWheel_withIronSpoke_withSteelRim_withTubelessTire_Dec_Month() {
        when(LocalDate.now().getMonthValue()).thenReturn(12);
        Wheel wheel = new Wheel();
        wheel.setSpokesType("iron");
        wheel.setNumberOfSpokes(numberOfSpokes);
        wheel.setRimThickness(rimThickness);
        wheel.setRimType("steel");
        wheel.setTyreType("tubeless");
        Double actualPrice = wheel.getWheelPrice();
        Double expectedPrice = 280.0;
        assertTrue(expectedPrice.equals(actualPrice));
    }

    @Test
    public void getWheel_withIronSpoke_withSteelRim_withTubeTire_Dec_Month() {
        when(LocalDate.now().getMonthValue()).thenReturn(12);
        Wheel wheel = new Wheel();
        wheel.setSpokesType("iron");
        wheel.setNumberOfSpokes(numberOfSpokes);
        wheel.setRimThickness(rimThickness);
        wheel.setRimType("steel");
        wheel.setTyreType("tube");
        Double actualPrice = wheel.getWheelPrice();
        Double expectedPrice = 250.0;
        assertTrue(expectedPrice.equals(actualPrice));
    }

    @Test
    public void getWheel_withIronSpoke_withIronRim_withTubelessTire_Dec_Month() {
        when(LocalDate.now().getMonthValue()).thenReturn(12);
        Wheel wheel = new Wheel();
        wheel.setSpokesType("iron");
        wheel.setNumberOfSpokes(numberOfSpokes);
        wheel.setRimThickness(rimThickness);
        wheel.setRimType("iron");
        wheel.setTyreType("tubeless");
        Double actualPrice = wheel.getWheelPrice();
        Double expectedPrice = 382.0;
        assertTrue(expectedPrice.equals(actualPrice));
    }

    @Test
    public void getWheel_withIronSpoke_withIronRim_withTubeTire_Dec_Month_Dec_Month() {
        when(LocalDate.now().getMonthValue()).thenReturn(5);
        Wheel wheel = new Wheel();
        wheel.setSpokesType("iron");
        wheel.setNumberOfSpokes(numberOfSpokes);
        wheel.setRimThickness(rimThickness);
        wheel.setRimType("iron");
        wheel.setTyreType("tube");
        Double actualPrice = wheel.getWheelPrice();
        Double expectedPrice = 352.0;
        assertTrue(expectedPrice.equals(actualPrice));
    }

}