package com.task.cyclepricingsolution.dto;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.DisplayName;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;

import static org.junit.Assert.*;

/**
 * @author Mahendra Prajapati
 * @since 19-08-2020
 */
public class ChainTest {

    @InjectMocks
    private Chain chain;

    {
        MockitoAnnotations.initMocks(this);
    }

    @Before
    public void setup() {
        if(chain == null) {
            chain = new Chain();
        } else {
            chain.setContainsGears(false);
            chain.setNumberOfGears(0);
        }
    }

    @Test
    @DisplayName("Contains Gears")
    public void chainWithGearsTest() {
        chain.setContainsGears(true);
        chain.setNumberOfGears(5);
        Double actualPrice = chain.getChainPrice();
        Double expectedPrice = 125.0;

        assertTrue(expectedPrice.equals(actualPrice));
    }

    @Test
    @DisplayName("Not Contains Gears")
    public void chainWithoutGearsTest() {
        chain.setContainsGears(false);
        Double actualPrice = chain.getChainPrice();
        Double expectedPrice = 20.0;

        assertTrue(expectedPrice.equals(actualPrice));
    }

    @After
    public void tearDown() {
        chain.setContainsGears(false);
        chain.setNumberOfGears(0);
    }

}