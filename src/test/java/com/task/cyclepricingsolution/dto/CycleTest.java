package com.task.cyclepricingsolution.dto;

import org.junit.Test;

import org.junit.jupiter.api.DisplayName;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.junit.Assert.*;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.mockito.Mockito.*;

/**
 * @author Mahendra Prajapati
 * @since 18-08-2020
 */
public class CycleTest {
    @InjectMocks
    private Cycle cycle;

    @Mock
    private Frame frame;

    @Mock
    private HandleBar handleBar;

    @Mock
    private Seat seat;

    @Mock
    private Wheel wheel;

    @Mock
    private Chain chain;

    {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    @DisplayName("All Component available")
    public void calculateCyclePrice_sucessCase() {
        when(frame.getFramePrice()).thenReturn(20.0);
        when(handleBar.getHandleBarPrice()).thenReturn(40.0);
        when(seat.getSeatPrice()).thenReturn(50.0);
        when(wheel.getWheelPrice()).thenReturn(50.0);
        when(chain.getChainPrice()).thenReturn(70.0);

        double actualCyclePrice = cycle.getCyclePrice();
        double expectedCyclePrice = 230.0;
        assertAll(
                () -> verify(frame, times(1)).getFramePrice(),
                () -> verify(handleBar, times(1)).getHandleBarPrice(),
                () -> verify(seat, times(1)).getSeatPrice(),
                () -> verify(wheel, times(1)).getWheelPrice(),
                () -> verify(chain, times(1)).getChainPrice(),

                () -> assertTrue(expectedCyclePrice == actualCyclePrice)
        );
    }

    @Test
    @DisplayName("Missing Frame")
    public void calculateCyclePrice_failCase_missingFrame() {
        cycle.setFrame(null);

        double actualCyclePrice = cycle.getCyclePrice();
        double expectedCyclePrice = 0.0;

        assertTrue(expectedCyclePrice == actualCyclePrice);
    }

    @Test
    @DisplayName("Missing Handle Bar")
    public void calculateCyclePrice_failCase_missingHandleBar() {
        cycle.setHandleBar(null);

        double actualCyclePrice = cycle.getCyclePrice();
        double expectedCyclePrice = 0.0;
        assertTrue(expectedCyclePrice == actualCyclePrice);
    }

    @Test
    @DisplayName("Missing Seat")
    public void calculateCyclePrice_failCase_missingSeat() {
        cycle.setSeat(null);

        double actualCyclePrice = cycle.getCyclePrice();
        double expectedCyclePrice = 0.0;
        assertTrue(expectedCyclePrice == actualCyclePrice);
    }

    @Test
    @DisplayName("Missing Wheel")
    public void calculateCyclePrice_failCase_missingWheel() {
        cycle.setWheel(null);

        double actualCyclePrice = cycle.getCyclePrice();
        double expectedCyclePrice = 0.0;
        assertTrue(expectedCyclePrice == actualCyclePrice);
    }

    @Test
    @DisplayName("Missing Chain")
    public void calculateCyclePrice_failCase_missingChain() {
        cycle.setChain(null);

        double actualCyclePrice = cycle.getCyclePrice();
        double expectedCyclePrice = 0.0;
        assertTrue(expectedCyclePrice == actualCyclePrice);
    }

}